\begin{appendices}
\section{Introduction}

We present implementation details in Sec.~\ref{sec:impl} and additional results that complement those shown in Section~\ref{sec:experiments}. These include input and output reconstructions in Sec.~\ref{sec:recons}.

\section{Implementation Details}
\label{sec:impl}

The main component requires for the proposed CompNN is to compute patch correspondences. To do this, we implemented a multi-threaded version of an exhaustive search method and the proposed HyperPatch-Match in C++ 11. For linear algebra operations we used Eigen library, and for generating visualizations we used OpenCV 3 via Python wrappers. 

\begin{table}
\caption{Pix2Pix patch and hyperpatch dimensions}
\setlength{\tabcolsep}{12pt}
\centering
\begin{tabular}{c c c}
\hline
Layer & Hyperpatch & Patch \\
\hline
Encoder 1 & $2 \times 2 \times 64$ & $2 \times 2$ \\
Encoder 2 & $2 \times 2 \times 128$ & $4 \times 4$ \\
Encoder 3 & $2 \times 2 \times 256$ & $8 \times 8$ \\
Encoder 4 & $2 \times 2 \times 512$ & $16 \times 16$ \\
Encoder 5 & $2 \times 2 \times 512$ & $32 \times 32$ \\
Encoder 6 & $2 \times 2 \times 512$ & $64 \times 64$ \\
Encoder 7 & $2 \times 2 \times 512$ & $128 \times 128$ \\
Encoder 7 & $2 \times 2 \times 512$ & $128 \times 128$ \\
Decoder 8 & $2 \times 2 \times 1024$ & $128 \times 128$ \\
Decoder 7 & $2 \times 2 \times 1024$ & $64 \times 64$ \\
Decoder 6 & $2 \times 2 \times 1024$ & $32 \times 32$ \\
Decoder 5 & $2 \times 2 \times 1024$ & $16 \times 16$ \\
Decoder 4 & $2 \times 2 \times 512$ & $8 \times 8$ \\
Decoder 3 & $2 \times 2 \times 256$ & $4 \times 4$ \\
Decoder 2 & $2 \times 2 \times 128$ & $2 \times 2$ \\
\hline
\end{tabular}
\label{tab:patch_dims}
\end{table}

We also present the patch sizes and hyperpatch dimensions used for the visualizations of the Pix2Pix embeddings. The parameters are shown in Table~\ref{tab:patch_dims}.

\section{Input and Output Reconstructions}
\label{sec:recons}

% 4.1
In this section, we present additional input and output reconstructions that complement the results shown in Section~\ref{sec:input_output_recon}. 
%%% Input Reconstructions.
\subsection{Input Reconstructions}

We show input reconstructions on two validation sets from the Facades and Cityscapes datasets. Similar to the input reconstructions shown in Fig.~\ref{fig:input_reconstructions}, we present reconstructions for the labels-to-images task on the Facades dataset in Fig.~\ref{fig:facades_input_recons}, images-to-labels task on the Cityscapes dataset in Fig.~\ref{fig:cityscapes_a2b_input_recon}, and labels-to-images task on the Cityscapes dataset in Fig.~\ref{fig:cityscapes_b2a_input_recon}. The structure of the Figures is the following: the first column presents the output to reconstruct, while the remaining columns present reconstruction from encoder and decoder layers. Overall, these results show that the first encoder layer (Encoder 1) and the last decoder layer (Decoder 2) produce the highest quality input reconstructions. Also, the reconstructions from layers closer to the bottleneck produce the reconstructions with a decreased quality. These results confirm that the proposed approach is able to reconstruct never-before-seen input images from the patch correspondences and the training set.

\begin{figure*}[t]
\centering
\includegraphics[width=\textwidth]{figures/facades_input_recon.pdf}
\caption{Input reconstructions for the labels-to-images task on the Facades dataset. We can observe that the first encoder layers (Encoder 1-2) preserve more information to reconstruct the input image with a good quality. On the other hand, the layers near the bottleneck (e.g., Encoder 3 and Decoder 4-3) lack information to provide a good quality input reconstruction. Note that the last decoder layer (Decoder 2) produces a good input reconstruction despite a few noisy artifacts.}
\label{fig:facades_input_recons}
\end{figure*}

\begin{figure*}[t]
\centering
\includegraphics[width=\textwidth]{figures/cityscapes_a2b_input_recon.pdf}
\caption{Input reconstructions for the images-to-labels task on the Cityscapes dataset. This dataset shows natural scenes with various objects (e.g., humans, cars, buildings). Despite the more complex scenes-to-reconstruct, we observe a similar reconstruction pattern shown in Fig.~\ref{fig:facades_input_recons}. The input reconstructions from Encoder 1 are of higher quality than that of the rest of the layers. The reconstructions decrease quality when they are close to the bottleneck of the network (e.g., Encoder 3, Decoder 4-3). However, we can also observe that the Decoder 2 layer delivers a reasonable reconstruction. We attribute this to the skip connections since they allow a direct transfer of information.}
\label{fig:cityscapes_a2b_input_recon}
\end{figure*}

\begin{figure*}[t]
\centering
\includegraphics[width=\textwidth]{figures/cityscapes_b2a_input_recon.pdf}
\caption{Input reconstructions for the labels-to-images task on the Cityscapes dataset. In this dataset we observe the same reconstruction pattern discussed in Fig.~\ref{fig:facades_input_recons} and Fig.~\ref{fig:cityscapes_a2b_input_recon}. We observe that the Encoder 1 layer again produces the highest quality of reconstruction. The Decoder 2 layer produces a comparable reconstruction to those of Encoder 1 thanks to the skip connections. Layers near the bottleneck again show a decrease in quality and we can observe block-like artifacts (see Decoder 4 and 3).}
\label{fig:cityscapes_b2a_input_recon}
\end{figure*}

%%% Output Reconstructions.
\subsection{Output Reconstructions}
We now show output reconstructions on two validation sets from the Facades and Cityscapes datasets. Similar to the output reconstructions shown in Fig.~\ref{fig:output_reconstructions}, we present reconstructions for the labels-to-images task on the Facades dataset in Fig.~\ref{fig:facades_output_recons}, images-to-labels task on the Cityscapes dataset in Fig.~\ref{fig:cityscapes_a2b_output_recons}, and labels-to-images task on the Cityscapes dataset in Fig.~\ref{fig:cityscapes_b2a_output_recons}. The structure of the Figures is the following: the first column presents the output to reconstruct, while the remaining columns present reconstruction from encoder and decoder layers. Different from the input reconstructions, the decoder layers produce better output reconstructions. In particular, the Decoder 2 layer produces the reasonable output reconstructions. On the other hand, the remaining decoder layers maintain the structure of the images but cannot reconstruct the output network image with great detail. Finally, the encoder layers have the least information to generate a plausible reconstruction of the output image of the network. Overall, these results confirm that CompNN is able to generate images that resemble the output of the Network from the patch correspondences and the training set, especially using information from the Decoder 2 layer.

\begin{figure*}[t]
\centering
\includegraphics[width=\textwidth]{figures/facades_output_recon.pdf}
\caption{Output reconstructions for the labels-to-images task on the Facades dataset. We can observe that the Decoder 2 and Decoder 3 layers produce plausible reconstructions of the output image. The remaining decoder layers struggle more to produce a reasonable reconstruction. Finally, the Encoder layers struggle the most in producing a reasonable reconstruction. We observe that the farther away from the output layer, the higher the struggle to produce a reasonable reconstruction. Note also that hallucination emerge from the Decoder layers. The results of the Decoder 4-2 layers in the first and fifth rows show the inclusion of clouds and blue sky.}
\label{fig:facades_output_recons}
\end{figure*}

\begin{figure*}[t]
\centering
\includegraphics[width=\textwidth]{figures/cityscapes_a2b_output_recon.pdf}
\caption{Output reconstructions for the images-to-labels task on the Cityscapes dataset. We can observe again that encoder layers struggle the most to reconstruct the output of the network. In contrast with the reconstructions shown in Fig.~\ref{fig:facades_output_recons}, CompNN produced reasonable reconstructions using information from Decoder 4 and 3. In this dataset, the reconstructions from Decoder 2 show noisy artifacts. We attribute this to the approximate nature of CompNN. Despite these artifacts, Decoder-2-based reconstructions resemble well the output of the network.}
\label{fig:cityscapes_a2b_output_recons}
\end{figure*}

\begin{figure*}[t]
\centering
\includegraphics[width=\textwidth]{figures/cityscapes_b2a_output_recon.pdf}
\caption{Output reconstructions for the labels-to-images task on the Cityscapes dataset. Once again, we can observe that the encoder layers have the least amount of information to reconstruct the structure of the output image well. However, the decoder layers have more information to generate details in the output image that make it closer to the output of the network. Surprisingly, the reconstructions from Decoder 4 and Decoder 3 look cleaner than that of the Decoder 2. The reconstructions of Decoder 2 present noisy artifacts due to the approximate nature of CompNN. Nevertheless, the images resemble well the output of the network.}
\label{fig:cityscapes_b2a_output_recons}
\end{figure*}
\end{appendices}
