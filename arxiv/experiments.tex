\section{Experiments}
\label{sec:experiments}

In this section, we present a series of qualitative and quantitative experiments to assess the input and output reconstructions, semantic correspondences, and property-controlling of the output reconstruction. For these experiments, we focus on analyzing embeddings learned by the Pix2Pix~
\cite{pix2pix2016} and SegNet~\cite{BadrinarayananK15} networks. The experiments consider image segmentation and image translation as the visual tasks to solve with the aforementioned networks.

\subsection{Input and Output Image Reconstructions}
\label{sec:input_output_recon}
In this section we present qualitative and quantitative experiments that assess the reconstructions for the input and output images of the underlying CNN. For these experiments we used the U-Net-based Pix2Pix~\cite{pix2pix2016} network. Note that the layers of the generator are referenced as Encoder 1-7 following with Decoder 7-1.

To get all the patch representations, we used the publicly available\footnote{Pix2Pix: \url{https://github.com/affinelayer/pix2pix-tensorflow}} pre-trained models for the facades and cityscapes datasets. Because we are interested in interpreting the embeddings that each of the layers of a CNN learn, we extracted all the patch representations for every encoder and decoder layers of the training and validation sets; see Sec.~\ref{sec:patch_representations} for details on how to extract patch representations from the activation tensors at each layer.

Given these patch representations, we used HyperPatch-Match to establish correspondences and reconstruct both input and output images, as described in Section~\ref{sec:patch_correspondences}. We used the top 16 global nearest neighbors to constrain the set used as the training set for every image in the validation set. To select these global nearest neighbors, we used the whole tensor from the Decoder-7 (bottleneck feature) as the global image descriptor. Also, since HyperPatch-Match is an iterative algorithm, we allowed it to run for 1024 iterations.

% Input reconstructions.
\begin{figure*}[t]
\centering
\includegraphics[width=\textwidth]{figures/input_recons.pdf}
\vspace{-4mm}
\caption{Input reconstructions using patch representations from encoder and decoder layers. We can observe that the first layers of the encoder and the last layer of the decoder produce a good reconstruction of the input. On the other hand, the reconstructions from the inner layers tend to maintain the structure of the input image, but the quality of the reconstructions decays as we get closer to the middle layers.}
\label{fig:input_reconstructions}
\vspace{-4mm}
\end{figure*}

The results of the input reconstructions are shown in Fig.~\ref{fig:input_reconstructions}. The first two rows show reconstructions for a labels-to-image task, while the bottom two rows show reconstructions for an image-to-labels task. The left column in this Figure shows the inputs to the network, and the remaining columns show reconstructions computed from three layers belonging to the encoder and decoder parts of the CNN. Given that Pix2Pix uses skips connections, i.e., the output of an encoder layer is concatenated to the output of a decoder layer to form the input of the subsequent decoder layer, Fig.~\ref{fig:input_reconstructions} shows reconstructions of the corresponding encoder and decoder layers. This means that the activations of Encoder 1 are part of the activations of Decoder 2.

We can observe in Fig.~\ref{fig:input_reconstructions} that the first layers of the encoder produce a good quality reconstruction of the input, an explanation for this observation is that much of the information to reconstruct the input is still present in the first encoder layer because only a layer of 2D convolutions has been applied. Surprisingly, the last layers of the decoder still produce good quality reconstructions. This can be attributed to the skip connections making both layers (Encoder 1 and Decoder 2) possess the same amount of information to reconstruct a good quality image. On the other hand, the reconstructions from the inner layers tend to maintain the structure of the input image, but the quality  of the reconstructions decays as we get closer to the middle of the U-Net architecture. We refer the reader to the supplemental material for additional input reconstructions. These results show that CompNN is a capable inversion-by-patch-correspondences method. 

% Output reconstructions.
\begin{figure*}[t]
\centering
\includegraphics[width=\textwidth]{figures/output_recons.pdf}
\vspace{-4mm}
\caption{Output reconstructions using patch representations from encoder and decoder layers. We can observe that the reconstructions from the encoder layers present an overall structure of the output image but lack details that the output image possess. On the other hand, the reconstructions of the decoder layer possess structural information and more details present in the output of the network.}
\label{fig:output_reconstructions}
\vspace{-4mm}
\end{figure*}

The results of the output reconstructions are shown in Fig.~\ref{fig:output_reconstructions}. The column in the left shows the output image generated by the underlying CNN. The organization of the remaining columns is the same as that of the Fig.~\ref{fig:input_reconstructions}. We can observe that the reconstructions from the encoder layers present an overall structure of the output image but lack details that the output image possess. For instance, consider the first row. The reconstructions from the encoder layers preserve the structure of the facade, i.e., the color and location of windows and doors. On the other hand, the reconstructions of the decoder layer possess structural information and more details present in the output of the network. For instance, in the first row, the output image contains a wedge depicting the sky. That part is present in the reconstructions of the decoder layers, but it is not present in the reconstructions of the encoder layers. Despite the fact that we use an approximation method to speed up the nearest neighbor search, we can observe that the reconstruction from the last decoder layer resembles well the image generated by the CNN. Note that sky details appear as well for the second row only in the decoder layers. This suggest that the hallucinations only emerge from the decoder layers of the U-net architecture. More results supporting this hypothesis can be seen in Fig.~\ref{fig:facades_output_recons} in the appendix.

\begin{figure*}[tbp]
\centering
\includegraphics[width=1\linewidth]{figures/fig_corres}
\vspace{-8mm}
\caption{\textbf{Correspondence map:} Given the input label mask on the top left, we show the ground-truth image and the output of Pix2Pix below. Why does Pix2Pix synthesize the peculiar red awning from the input mask? To provide an explanation, we use CompNN to synthesize an image by explicitly cutting-and-pasting (composing) patches from training images. We color code pixels in the training images to denote correspondences. For example, CompNN copies doors from training image A (blue) and the red awning from training image C (yellow).}
\label{fig:corres_map}
\vspace{-4mm}
\end{figure*}

To interpret the synthesized outputs of CNNs for pixel-level tasks, we show a correspondence map in Fig.~\ref{fig:corres_map} illustrating that CompNN is a good tool for interpreting the outputs of the network. This Figure shows an example of labels-to-image task. The organization of the Figure is the following, in the left column we show the input, ground truth or target image, and the output of the network. In the second column, we color coded the correspondences found that contribute to the synthesis of the output image (shown in the last row in that column). The remaining columns show the different source images that contribute to the composition of the output image. These results suggest that the network learns an embedding that enables a rapid search of source patches that can be used to synthesize a final output. In other words, the embedding encodes patches that can be composed together to synthesize an output image.

Unlike existing interpretation methods that aim to reconstruct the input image only, CompNN is also capable of synthesizing an output image that resembles the generated image by the CNN. This is an important feature because it allows us to interpret the embeddings in charge of generating the synthesized image. Moreover, these results show that the smoothness variation of natural scenes is still present in the activation tensors of both encoder and decoder layers. Thanks to this property, CompNN is able to compute correspondences efficiently using HyperPatch-Match and compose an output image.


To evaluate the reconstructions in a quantitative way, we use the reconstructions from images to labels and assess them by Mean Pixel Accuracy and Mean Intersection over Union (IoU). For this experiment, we consider Pix2Pix on Facades and Cityscapes datasets as well as SegNet~\cite{BadrinarayananK15} on the Camvid dataset for the images-to-labels task. Also, we used HyperPatch-Match to compute correspondences for the Pix2Pix representations, and used an exhaustive search to compute the correspondences for the SegNet representations. The synthesized labeled-images used patch representations from the Decoder 2 layer for the Pix2Pix network, and Decoder 4 or the last layer before the softmax layer for the SegNet network. We trained a SegNet model for the Camvid dataset using a publicly available tensorflow implementation~\footnote{SegNet Implementation: \url{https://github.com/tkuanlun350/Tensorflow-SegNet}}.

The results for the output reconstructions are shown in Table~\ref{tab:output_reconstruction}. The rows of the Table show from top to bottom the metrics for the CNN synthesized image (the baseline); GR (Bottleneck), a reconstruction which simply returns the most similar image in the training set using as global feature the bottleneck activation tensor; GR (FC7), a global reconstructions using the whole activation tensor of the penultimate layer; and output reconstructions (OR) using top 1, 16, 32, and 64 global images as the constrained training set for CompNN and HyperPatch-Match. The result for Camvid dataset which uses exhaustive search is placed in the OR (top 1) row. The entries in the Table show the metric differences between the reconstructions and the baseline, and we show in bold the numbers that are closest to the baseline. We can observe in Table~\ref{tab:output_reconstruction} that the compositional reconstructions overall tend to be close enough to those of the baseline. This is because the absolute value of the differences is small overall. Moreover, we can observe that considering more images in the training set for HyperPatch-Match tends to improve results; compare Facades and Cityscapes columns.

\begin{table}[t]
\caption{Quantitative evidence that CompNN can reconstruct very similar output images when compared with those of the network. The Table shows metric differences between the output of the CNN (the baseline) and CompNN reconstructions (OR) and global reconstructions (GR), which simply return the most similar output image from the training set. Bold entries indicate the closest reconstruction to the baseline.}
\vspace{-4mm}
\setlength{\tabcolsep}{3pt}
\def\arraystretch{1.2}
\center

\scriptsize
\begin{tabular}{c| c| c| c| c| c| c}
\hline
\multirow{2}{*}{Approach}  

& \multicolumn{3}{c|}{\em{(Mean Pixel Accuracy)}} & \multicolumn{3}{c}{\em{(Mean IoU)}} \\

\cline{2-7}
&  Facades & Cityscapes &  Camvid &  Facades & Cityscapes &  Camvid\\

\hline
Baseline CNN & 0.5105 & 0.7618 &  0.7900  & 0.2218 & 0.2097 & 0.4443 \\
\hline
\hline
GR (Bottleneck)  & -0.1963 & -0.1488 & -0.2200 & -0.1437 & -0.0735 & -0.1981 \\
\hline
GR (FC7) & -0.1730 & -0.1333 & -0.1263 & -0.1126 & -0.0702 & -0.1358 \\
\hline
OR (top 1)   & \bf{-0.0102} & -0.0545 & \bf{-0.0350} & -0.0253 &  -0.0277 & \bf{-0.0720} \\
\hline
OR (top 16)  & +0.0324 & -0.0218 & -- & \bf{+0.0214} & +0.0014& -- \\
\hline
OR (top 32)  & +0.0336 & -0.0182 & -- & +0.0232  & \bf{+0.0011} & -- \\
\hline
OR (top 64)  & +0.0343 & \bf{-0.0171} & -- & +0.0246 & +0.0020 & -- \\
\hline
\end{tabular}
\quad\quad\quad
\label{tab:output_reconstruction}
\end{table}


\begin{table}[t]
\caption{Mean Pixel Accuracy (MPA) for input reconstructions. We compare the input reconstructions with the original input label images. These results show that there is a good agreement between the input reconstructions and the original input label images.}
\vspace{-3mm}
\setlength{\tabcolsep}{3pt}
\def\arraystretch{1.2}
\center
\scriptsize
\begin{tabular}{c| c| c | c | c }
\hline
Approach &  IR (top 1)  & IR (top 16) & IR (top 32) & IR (top 64)   \\
\hline
Facades  & 0.723 & 0.837 & 0.844 & 0.846\\
\hline 
Cityscapes & 0.816 & 0.894 & 0.898 & 0.901\\
\hline
\end{tabular} \quad\quad\quad
\label{tab:input_reconstruction}
\end{table}

To evaluate the input reconstructions, we utilized a similar approach used to evaluate the output image reconstructions. However, in this case we compared the input reconstructions with the original input label images only for a Pix2Pix network for labels-to-images task on Facades and Cityscapes datasets. We computed their agreement by means of the Mean Pixel Accuracy (MPA) metric, and we show the results in Table~\ref{tab:input_reconstruction}. Note that MPA compares class labels assigned to every pixel. We can observe that the MPA is overall high ($> 0.7$). In particular, we can observe that considering more top $k$ images in the training set for HyperPatch-Match increases the similarity between the reconstruction and the original input image.

\subsection{Semantic Correspondences and Property-Control in the Output Image}
\label{sec:correspondences}
\begin{figure*}[t]
\centering
\includegraphics[width=\textwidth]{figures/bias.pdf}
\vspace{-6mm}
\caption{{\bf (a)} We compute semantic correspondences between pairs of images. To visualize the correspondences, we color-code patches by their semantic label. In general, building patches from one image tend to match building patches from another, and similarly for vegetation. {\bf (b)} We can control the color and window properties of the facades. This feature can be used to understand the bias present in CNNs image outputs and can help users to fix these biases.}
\label{fig:bias}
\vspace{-4mm}
\end{figure*}

Fig.~\ref{fig:bias}(a) shows that HyperPatch-Match can be used to compute semantic correspondences between a pair of images. To show this is possible, we used the patch representations learned by SegNet for the images-to-labels task. To establish the semantic patch correspondences given a pair of images, we first extracted their patch representations from the Decoder 4 layer. Then, we computed the patch correspondences using HyperPatch-Match with 1024 maximum iterations. To visualize the correspondences, we color code patches with their semantic SegNet class. In general, building regions from one image tend to match to building regions from another, and likewise for vegetation.

An additional application of the patch-correspondences is that of controlling properties in the output image. That is, we can control properties (e.g., color of a facade) by simply modifying the images contained in the training set used for computing patch correspondences. To illustrate this, we show results on two different facades output reconstructions in Fig.~\ref{fig:bias}(b). We can observe in both cases that the color of the facades can be manipulated by simply selecting images in the training set depicting facades with the desired color. Also, note that the window frames and sidings changed color and in some cases the type. For instance, in the first row, the third image from left to right, shows windows with white frames and of different style than that of the network output. This control property can help users to understand the bias that CNNs present, and can help users to possibly correct it. See appendix material for additional results.
