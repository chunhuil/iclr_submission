% last updated in April 2002 by Antje Endemann
% Based on CVPR 07 and LNCS, with modifications by DAF, AZ and elle, 2008 and AA, 2010, and CC, 2011; TT, 2014; AAS, 2016

\documentclass[runningheads]{llncs}
\usepackage{graphicx}	
\usepackage{amsmath,amssymb} % define this before the line numbering.
\usepackage{ruler}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{color}
\usepackage{cite}
\usepackage[width=122mm,left=12mm,paperwidth=146mm,height=193mm,top=12mm,paperheight=217mm]{geometry}
\begin{document}
% \renewcommand\thelinenumber{\color[rgb]{0.2,0.5,0.8}\normalfont\sffamily\scriptsize\arabic{linenumber}\color[rgb]{0,0,0}}
% \renewcommand\makeLineNumber {\hss\thelinenumber\ \hspace{6mm} \rlap{\hskip\textwidth\ \hspace{6.5mm}\thelinenumber}}
% \linenumbers
\pagestyle{headings}
\mainmatter
\def\ECCV18SubNumber{2306}  % Insert your submission number here

\newcommand{\deva}[1]{{\color{blue} \bf [Deva:  #1]}}
\newcommand{\vf}[1]{{\color{red} \bf [VF:  #1]}}
\newcommand{\lch}[1]{{\color{orange} \bf [Chunhui:  #1]}}
\newcommand{\ab}[1]{{\color{green}\bf [Aayush: #1]}}	


% type user-defined commands here
\renewcommand{\thesection}{\Roman{section}} 
\renewcommand{\thesubsection}{\thesection.\Roman{subsection}}
\renewcommand{\thefigure}{\roman{figure}}

\title{Patch Correspondences for Interpreting Pixel-level CNNs \\ Supplemental Material} % Replace with your title

\titlerunning{ECCV-18 submission ID \ECCV18SubNumber}

\authorrunning{ECCV-18 submission ID \ECCV18SubNumber}

\author{Anonymous ECCV submission}
\institute{Paper ID \ECCV18SubNumber}

\maketitle

\section{Introduction}

We present implementation details in Sec.~\ref{sec:impl} and additional results that complement those shown in Section 4 in the main submission. These include input and output reconstructions in Sec.~\ref{sec:recons}, semantic correspondences in Sec.~\ref{sec:corres}, and property control in the output image in Sec.~\ref{sec:prop}.

\section{Implementation Details}
\label{sec:impl}

The main component requires for the proposed CompNN is to compute patch correspondences. To do this, we implemented a multi-threaded version of an exhaustive search method and the proposed HyperPatch-Match in C++ 11. For linear algebra operations we used Eigen library, and for generating visualizations we used OpenCV 3 via Python wrappers. We will release these implementations upon publication.

\begin{table}
\caption{Pix2Pix patch and hyperpatch dimensions}
\setlength{\tabcolsep}{12pt}
\centering
\begin{tabular}{c c c}
\hline
Layer & Hyperpatch & Patch \\
\hline
Encoder 1 & $2 \times 2 \times 64$ & $2 \times 2$ \\
Encoder 2 & $2 \times 2 \times 128$ & $4 \times 4$ \\
Encoder 3 & $2 \times 2 \times 256$ & $8 \times 8$ \\
Encoder 4 & $2 \times 2 \times 512$ & $16 \times 16$ \\
Encoder 5 & $2 \times 2 \times 512$ & $32 \times 32$ \\
Encoder 6 & $2 \times 2 \times 512$ & $64 \times 64$ \\
Encoder 7 & $2 \times 2 \times 512$ & $128 \times 128$ \\
Encoder 7 & $2 \times 2 \times 512$ & $128 \times 128$ \\
Decoder 8 & $2 \times 2 \times 1024$ & $128 \times 128$ \\
Decoder 7 & $2 \times 2 \times 1024$ & $64 \times 64$ \\
Decoder 6 & $2 \times 2 \times 1024$ & $32 \times 32$ \\
Decoder 5 & $2 \times 2 \times 1024$ & $16 \times 16$ \\
Decoder 4 & $2 \times 2 \times 512$ & $8 \times 8$ \\
Decoder 3 & $2 \times 2 \times 256$ & $4 \times 4$ \\
Decoder 2 & $2 \times 2 \times 128$ & $2 \times 2$ \\
\hline
\end{tabular}
\label{tab:patch_dims}
\end{table}

We also present the patch sizes and hyperpatch dimensions used for the visualizations of the Pix2Pix embeddings. The parameters are shown in Table~\ref{tab:patch_dims}.

\section{Input and Output Reconstructions}
\label{sec:recons}

In this section, we present additional input and output reconstructions that complement the results shown in Section 4.1 in the main submission. 
%%% Input Reconstructions.
\subsection{Input Reconstructions}

We show input reconstructions on two validation sets from the Facades and Cityscapes datasets. Similar to the input reconstructions shown in Fig. 5 in the main submission, we present reconstructions for the labels-to-images task on the Facades dataset in Fig.~\ref{fig:facades_input_recons}, images-to-labels task on the Cityscapes dataset in Fig.~\ref{fig:cityscapes_a2b_input_recon}, and labels-to-images task on the Cityscapes dataset in Fig.~\ref{fig:cityscapes_b2a_input_recon}. The structure of the Figures is the following: the first column presents the output to reconstruct, while the remaining columns present reconstruction from encoder and decoder layers. Overall, these results confirm that the proposed approach is able to reconstruct never-before-seen input images from the patch correspondences and the training set.

\begin{figure}[t]
\centering
\includegraphics[width=\textwidth]{figures/facades_input_recon.pdf}
\caption{Input reconstructions for the labels-to-images task on the Facades dataset. We can observe that the first encoder layers have information to reconstruct the input image. However, thanks to the skip connections, the last layers of the decoder also can reconstruct the input image well.}
\label{fig:facades_input_recons}
\end{figure}

\begin{figure}[t]
\centering
\includegraphics[width=\textwidth]{figures/cityscapes_a2b_input_recon.pdf}
\caption{Input reconstructions for the images-to-labels task on the Cityscapes dataset. We can observe that the first encoder layers have information to reconstruct the input image. However, thanks to the skip connections, the last layers of the decoder also can reconstruct the input image well.}
\label{fig:cityscapes_a2b_input_recon}
\end{figure}

\begin{figure}[t]
\centering
\includegraphics[width=\textwidth]{figures/cityscapes_b2a_input_recon.pdf}
\caption{Input reconstructions for the labels-to-images task on the Cityscapes dataset. We can observe that the first encoder layers have information to reconstruct the input image. However, thanks to the skip connections, the last layers of the decoder also can reconstruct the input image well.}
\label{fig:cityscapes_b2a_input_recon}
\end{figure}

%%% Output Reconstructions.
\subsection{Output Reconstructions}
We now show output reconstructions on two validation sets from the Facades and Cityscapes datasets. Similar to the output reconstructions shown in Fig. 6 in the main submission, we present reconstructions for the labels-to-images task on the Facades dataset in Fig.~\ref{fig:facades_output_recons}, images-to-labels task on the Cityscapes dataset in Fig.~\ref{fig:cityscapes_a2b_output_recons}, and labels-to-images task on the Cityscapes dataset in Fig.~\ref{fig:cityscapes_b2a_output_recons}. The structure of the Figures is the following: the first column presents the output to reconstruct, while the remaining columns present reconstruction from encoder and decoder layers. Overall, these results confirm that the network is able to generate images that resemble the output of the Network from the patch correspondences and the training set.

\begin{figure}[t]
\centering
\includegraphics[width=\textwidth]{figures/facades_output_recon.pdf}
\caption{Output reconstructions for the labels-to-images task on the Facades dataset. We can observe that the encoder layers have information to reconstruct the structure of the output image well. However, the decoder layers have more information to generate details in the output image that make it closer to the output of the network.}
\label{fig:facades_output_recons}
\end{figure}

\begin{figure}[t]
\centering
\includegraphics[width=\textwidth]{figures/cityscapes_a2b_output_recon.pdf}
\caption{Output reconstructions for the images-to-labels task on the Cityscapes dataset. We can observe that the encoder layers have information to reconstruct the structure of the output image well. However, the decoder layers have more information to generate details in the output image that make it closer to the output of the network.}
\label{fig:cityscapes_a2b_output_recons}
\end{figure}

\begin{figure}[t]
\centering
\includegraphics[width=\textwidth]{figures/cityscapes_b2a_output_recon.pdf}
\caption{Output reconstructions for the labels-to-images task on the Cityscapes dataset. We can observe that the encoder layers have information to reconstruct the structure of the output image well. However, the decoder layers have more information to generate details in the output image that make it closer to the output of the network.}
\label{fig:cityscapes_b2a_output_recons}
\end{figure}

\section{Semantic Correspondences}
\label{sec:corres}

\begin{figure}[t]
\centering
\includegraphics[width=\textwidth]{figures/camvid_corres.pdf}
\caption{Semantic correspondences between a query and training image from the Camvid dataset using SegNet features. Each row visualizes the patch correspondences that link patches in the query image with patches in the training image for Sky, Building, Road, and Sidewalk.}
\label{fig:camvid_correspondences}
\end{figure}

\begin{figure}[t]
\centering
\includegraphics[width=\textwidth]{figures/cityscapes_a2b_fig1.pdf}
\caption{Semantic correspondences between a query and training image from the Cityscapes dataset using Pix2Pix features. Each row visualizes the patch correspondences that link patches in the query image with patches in the training image for Sky, Flat, Nature, and Vehicles.}
\label{fig:cityscapes_a2b_correspondences_1}
\end{figure}

\begin{figure}[t]
\centering
\includegraphics[width=\textwidth]{figures/cityscapes_a2b_fig2.pdf}
\caption{Semantic correspondences between a query and training image from the Cityscapes dataset using Pix2Pix features. Each row visualizes the patch correspondences that link patches in the query image with patches in the training image for Sky, Flat, Nature, and Vehicles.}
\label{fig:cityscapes_a2b_correspondences_2}
\end{figure}

In this section, we present additional results to those shown in Section 4.2 of the main submission. In Fig.~\ref{fig:camvid_correspondences} we present patch correspondences using SegNet features on an image pair from the Camvid dataset; and in Fig.~\ref{fig:cityscapes_a2b_correspondences_1} and Fig.~\ref{fig:cityscapes_a2b_correspondences_2} we present patch correspondences using Pix2Pix features on an image pair from the Cityscapes datasets. The results show that these patch representations are rich enough to establish image-to-image correspondences based on semantic information.

\section{Property-Control in the Output Image}
\label{sec:prop}

\begin{figure}[t]
\centering
\includegraphics[width=\textwidth]{figures/prop_mods.pdf}
\caption{By simply modifying the images contained in the training set, CompNN is able to modify properties like color and texture of a facade. This feature can help users to understand the bias present in CNNs and possibly correct it.}
\label{fig:prop_mods}
\end{figure}

Lastly, we show additional results on controlling properties of the output image by modifying the images in the training set. The additional results shown in Fig.~\ref{fig:prop_mods} complement those shown in Fig. 8(b) in the main submission. We can show that by simply modifying the images in the training set, we can control color of the facade and windows (see second row and third row), and the facade style (e.g., red-brick in first row). This feature can help users to understand the bias present in CNNs and possibly correct it.

%\bibliographystyle{splncs}
%\bibliography{compnn}
\end{document}
