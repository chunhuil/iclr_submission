% last updated in April 2002 by Antje Endemann
% Based on CVPR 07 and LNCS, with modifications by DAF, AZ and elle, 2008 and AA, 2010, and CC, 2011; TT, 2014; AAS, 2016

\documentclass[runningheads]{llncs}
\usepackage{graphicx}	
\usepackage{amsmath,amssymb} % define this before the line numbering.
\usepackage{ruler}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{color}\setlength{\tabcolsep}{12pt}\setlength{\tabcolsep}{12pt}
\usepackage{cite}
\usepackage[width=122mm,left=12mm,paperwidth=146mm,height=193mm,top=12mm,paperheight=217mm]{geometry}
\begin{document}
% \renewcommand\thelinenumber{\color[rgb]{0.2,0.5,0.8}\normalfont\sffamily\scriptsize\arabic{linenumber}\color[rgb]{0,0,0}}
% \renewcommand\makeLineNumber {\hss\thelinenumber\ \hspace{6mm} \rlap{\hskip\textwidth\ \hspace{6.5mm}\thelinenumber}}
% \linenumbers
\pagestyle{headings}
\mainmatter
\def\ECCV18SubNumber{2306}  % Insert your submission number here

\newcommand{\deva}[1]{{\color{blue} \bf [Deva:  #1]}}
\newcommand{\vf}[1]{{\color{red} \bf [VF:  #1]}}
\newcommand{\lch}[1]{{\color{orange} \bf [Chunhui:  #1]}}
\newcommand{\ab}[1]{{\color{green}\bf [Aayush: #1]}}	

\renewcommand{\thefigure}{\Alph{figure}}


\title{Patch Correspondences for Interpreting Pixel-level CNNs} % Replace with your title

\titlerunning{ECCV-18 submission ID \ECCV18SubNumber}

\authorrunning{ECCV-18 submission ID \ECCV18SubNumber}

\author{Anonymous ECCV submission}
\institute{Paper ID \ECCV18SubNumber}

\maketitle

%%%
% TODO(vfragoso): Write an insight and state that you will include it in a final version.
\vspace{-4mm}
We thank the reviewers for their time and feedback. We are pleased to see that most reviewers find the proposed CompNN as a useful tool to gain insights into pixel-level CNNs. Below are our responses to your concerns.
\vspace{-7mm}
\subsubsection*{Novelty of Approach (R2 \& R3).}
%\vspace{-2mm} 
{\bf R2} believes that the work is not new. This belief comes from an assumption that the proposed CompNN is a universal image synthesizer that competes with \cite{deep_analogy, li2016combining, yang2017high}. However, as correctly noted by {\bf R1} and {\bf R3} and stated in lines 9-12 and 80-99 of the main submission, CompNN's goal is to reconstruct the input and output images for the purpose of {\bf interpreting} the embeddings of pixel-level CNNs as in ~\cite{mahendran2016visualizing}. Consequently, {\bf R2}'s comparisons between results of universal image synthesis (e.g., \cite{deep_analogy, li2016combining, yang2017high}) and those of CompNN are not fair comparisons. We will emphasize this more in a final version.

\noindent {\bf R3} is concerned about the novelty of CompNN. Unlike existing visual interpretation methods~\cite{mahendran2016visualizing, vondrick2013hoggles} that learn functions to invert a representation in order to reconstruct an input image, CompNN uses an example-based approach that does not require a training phase to learn a function to invert a representation; to the best of our knowledge, CompNN is the first example-based inversion method. Also, we demonstrate that CompNN can reconstruct not only the input image but also the output image.

% \noindent {\bf R2} is concerned because there are no contributions to solve the NN-search problem. As explained earlier, the focus was to develop a method for interpretation.

\vspace{-6mm}
\subsubsection*{Results (R1, R2, R3).}
%\vspace{-2mm} 
{\bf R1} proposes to expand semantic diversity in the experiments. We agree and we'll leave this for future work. However, the  experiments still present a fair amount of semantic diversity; the {\it facades}, {\it cityscapes}, and {\it camvid} datasets have images containing various objects in the wild.

\noindent {\bf R3} is concerned about the novelty of the results on dense correspondences and transfering labels shown in Sec. 4.2. We don't claim that we introduced these applications. Instead, we only show that HyperPatch-Match combined with the embeddings of a pixel-level CNN can be used to establish dense correspondences and solve those applications. We will clarify this in a final section.

\noindent {\bf R2} is concerned that no insights were discussed. The main insights that CompNN reveals about pixel-level CNNs are: 1) every layer can produce an embedding containing information about the input and output images that can be used as patch representations; 2) outer layers (first encoder and last decoder layers) possess the most information about the input and output images, respectively; 3) layers near the bottleneck possess the least information about the input and output images; 4) patch representations obtained from the pixel-level CNNs vary smoothly; and 5) the decoder part hallucinates information -- see the first row of Fig. 6 which shows that the decoder layers add the triangular portion depicting the sky above the building. We will emphasize this in a final version.

\begin{figure}[t]
\centering
\includegraphics[width=0.85\textwidth]{figures/rebuttal_fig.pdf}
\vspace{-4mm}
\caption{Input reconstruction using~\cite{mahendran2016visualizing} (middle row) and CompNN (bottom row).}
\vspace{-6mm}
\label{fig:rebuttal}
\end{figure}
\noindent {\bf R3} and {\bf R2} think the results are noisy and bad, respectively. Results can improve by using~\cite{PixelNN}. However, the results from the interpretation perspective show that CompNN outperforms~\cite{mahendran2016visualizing}; see Fig.~\ref{fig:rebuttal}. We'll add this comparison in a final version.

\vspace{-6mm}
\subsubsection*{Clarifications of Approach (R1 \& R3).}
%\vspace{-2mm}
{\bf R1} asks how large a dataset needs to be to produce insightful results. Experimentally, we determined that $k \in \left[16, 64\right]$ produces an image that resembles well the output of the CNN; e.g., Figs. 5 and 6 used $k=16$. A small dataset produces results with the correct semantic (i.e., a door or window is placed where it is expected), but the texture information may not replicate the one produced by the CNN; e.g., Fig. 8b and Fig. X used $k \leq 4$. The larger the value of $k$, the more similar the synthesized image to the one produced by the CNN. We'll clarify this in a final version.

% In fact, the results in Fig. 8b in the main submission and Fig. X in the supp. material used $k \leq 4$, while results in Figs. 5 and 6 used $k = 16$. 

\noindent {\bf R3} asks if the reconstruction of a pixel is done independently. No. This is because a neighboring pixel can inform about potential solutions for a query pixel, which is what the propagation step does (see Sec. 3.2).

\noindent {\bf R3} asks how the output image is reconstructed given the correspondences.  The output value (RGB or label) of a pixel is a copy of the corresponding pixel value in a training image. A patch is only used to get the descriptor for a single pixel. Consequently, there is no overlap of patches in the synthesis process. 

\noindent {\bf R1} asks if at some point CompNN becomes infeasible. As $k$ gets very large, the convergence of CompNN slows down. Many factors contribute to this slowdown, e.g., disk i/o, dataset size, and hardware. Disk i/o slowed down the most but we mitigated this using an LRU cache. We'll discuss this in a final version.

\vspace{-6mm}
\tiny{
\bibliographystyle{splncs}
\bibliography{rebuttal}
}
\end{document}
